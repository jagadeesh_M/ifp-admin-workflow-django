# syntax=docker/dockerfile:1
FROM python:slim-bullseye
ENV PYTHONDONTWRITEBYTECODE=1
ENV PYTHONUNBUFFERED=1
WORKDIR /code
COPY requirements.txt /code/
RUN apt update
RUN pip install -r requirements.txt
COPY . /code/
